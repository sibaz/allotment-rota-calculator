<?php
  require_once(dirname(__FILE__).'/datafiles.php');
 
  define('DATA_FILE', dirname(dirname(__FILE__)).'/data/data.json');

  $data = json_decode(file_get_contents(DATA_FILE), true);
  $rotaMembersFile = file_get_contents(ROTA_MEMBERS_FILE);
  $rotaMembers = json_decode($rotaMembersFile, true);

  //var_dump($rotaMembers);
  //var_dump($data);

  $members = $data['members'];
  $positions = $data['positions'];
  //var_dump($members);
  //var_dump($positions);

  $newmembers = array();
  $membersindex = array();

  foreach ($members as $index => $member) {
    $newmember = array('name'=>trim($member), 'uid'=>(100+$index), 'email'=>'', 'shopvolunteer'=>true);
    $newmembers[$newmember['uid']] = $newmember;
    $membersindex[$newmember['uid']] = $newmember['name'];
  }
  //var_dump($newmembers);

  $newpositions = array();
  foreach ($positions as $position => $member) {
    $holder = array_search(trim($member), $membersindex);
    $newpositions[] = array('position'=>$position, 'holder'=>$holder);
  }
  foreach ($newpositions as $index => $position) {
    $newpositions[$index]['keyposition'] = ($index<13);
  }
  //var_dump($newpositions);

  //$keyholders=array();
  //$nonkeyholders=array();
  foreach ($newpositions as $position) {
	$holderID = $position['holder'];
    if (empty($holderID)) {
      continue;
    }
	$member = $newmembers[$holderID];
    $newmembers[$holderID]['keyholder']=$position['keyposition'];
    /*
    if ($position['keyposition']) {
		if ($member['shopvolunteer']) {
        	$keyholders[] = $holderID;
		}
      //$keyholders[$holderID]=array('shopvolunteer'=>$member['shopvolunteer']);
    } else {
		if ($member['shopvolunteer']) {
      		$nonkeyholders[] = $holderID;
		}
      //$nonkeyholders[$holderID]=array('shopvolunteer'=>true);
    }
	*/
  }
  //$rotamembers = array('keyholders'=>$keyholders, 'nonkeyholders'=>$nonkeyholders);
  file_put_contents(MEMBERS_FILE, json_encode($newmembers, JSON_PRETTY_PRINT));
  file_put_contents(POSITIONS_FILE, json_encode($newpositions, JSON_PRETTY_PRINT));
  //file_put_contents(ROTA_MEMBERS_FILE, json_encode($rotamembers, JSON_PRETTY_PRINT));
  //var_dump($newmembers);
  //var_dump($keyholders);
  //var_dump($nonkeyholders);
  echo "<pre>\n"; echo file_get_contents(MEMBERS_FILE); echo "</pre>\n";
  echo "<pre>\n"; echo file_get_contents(POSITIONS_FILE); echo "</pre>\n";
  //echo "<pre>\n"; echo file_get_contents(ROTA_MEMBERS_FILE); echo "</pre>\n";

