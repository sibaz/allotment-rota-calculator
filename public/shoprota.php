<?php
  require_once(dirname(__FILE__).'/datafiles.php');

  $startdate = "2019-10-20";

  $members = json_decode(file_get_contents(MEMBERS_FILE), true);
  $positions = json_decode(file_get_contents(POSITIONS_FILE), true);
  $rotafile = json_decode(file_get_contents(ROTA_MEMBERS_FILE), true);
  $keyholders = $rotafile['keyholders'];
  $nonkeyholders = $rotafile['nonkeyholders'];
  $shopduty = $rotafile['shopduty'];
  /*
  $keyholders=array();
  $nonkeyholders=array();
  foreach ($positions as $position) {
	$holderID = $position['holder'];
    if (empty($holderID)) {
      continue;
    }
	$member = $members[$holderID];
    if ($position['keyposition']) {
		if ($member['shopvolunteer'] && !in_array($holderID, $keyholders)) {
        	$keyholders[] = $holderID;
		}
      //$keyholders[$holderID]=array('shopvolunteer'=>$member['shopvolunteer']);
    } else {
		if ($member['shopvolunteer'] && !in_array($holderID, $nonkeyholders)) {
      		$nonkeyholders[] = $holderID;
		}
      //$nonkeyholders[$holderID]=array('shopvolunteer'=>true);
    }
  }
  */
  echo "<h1>Keyholders</h1>\n";
  echo "<ul>\n";
  foreach($keyholders as $member) {
    echo "<li>".$members[$member]['name']."\n";
  }
  echo "</ul>\n";
  echo "<h1>Non-keyholders</h1>\n";
  echo "<ul>\n";
  foreach($nonkeyholders as $member) {
    echo "<li>".$members[$member]['name']."\n";
  }
  echo "</ul>\n";

  echo "<h1>Rota</h1>\n";
  /*
  $startunixdate=strtotime($startdate);
  $nkeyholders = count($keyholders);
  $nnonkeyholders = count($nonkeyholders);
  $shopduty = array();
  for ($week=0;$week<41;$week++) {
    $keyholder = $keyholders[$week%$nkeyholders];
	$nonkeyholder = $nonkeyholders[$week%$nnonkeyholders];
    $unixtime = strtotime("+".$week."week", $startunixdate);
    $fulldate = strftime("%Y %B %d", $unixtime);
    $month = strftime("%m", $unixtime);
    $note="";
    if ($month == 3 || $month == 4) {
      $note = " (Opening hours 10-12)";
    }
    echo "Week: ".($week+1)." - $fulldate$note: ".$members[$keyholder]['name'].", ".$members[$nonkeyholder]['name']." <br>\n";
    $shopduty[] = array('date'=>$fulldate, 'unixdate'=>$unixtime, 'keyholder'=>$keyholder, 'nonkeyholder'=>$nonkeyholder);
  }
  */
  foreach($shopduty as $dutyday) {
    $fulldate = $dutyday['date'];
    $unixdate = $dutyday['unixdate']; 
    $keyholder = $dutyday['keyholder'];
    $nonkeyholder = $dutyday['nonkeyholder'];
    $month = strftime("%m", $unixtime);
    $note="";
    if ($month == 3 || $month == 4) {
      $note = " (Opening hours 10-12)";
    }
    echo "Week: ".($week+1)." - $fulldate$note: ".$members[$keyholder]['name'].", ".$members[$nonkeyholder]['name']." <br>\n";
  }

